﻿using System;

namespace Module_4.Libs
{
    class Task_1Lib
    {

        static Random random = new Random();

        /// <summary>
        /// Метод для инициализации массивов рандомными значениями
        /// </summary>
        /// <returns>Массив с рандомными данными</returns>
        public static int[] GenerateRandomNumbersArray(int monthsCount)
        {
            int[] array = new int[monthsCount];

            for (int i = 0; i < monthsCount; i++)
                array[i] = random.Next(1, 9);

            return array;
        }

    }
}
