﻿namespace Module_4.Libs
{
    class Task_2Libs
    {

        /// <summary>
        /// Метод подсчета факториала
        /// </summary>
        public float Factorial(int number)
        {
            float result = 1;

            for (int i = 1; i <= number; i++)
                result *= i;

            return result;
        }
    }
}
