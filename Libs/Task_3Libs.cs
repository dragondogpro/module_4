﻿using System;

namespace Module_4.Libs
{
    class Task_3Libs
    {
        Random random = new Random();

        public double[,] GetMatrixNxN(int col, int row)
        {
            double[,] matrix = new double[col, row];

            for (int i = 0; i < col; i++)
            {
                for (int j = 0; j < row; j++)
                {
                    matrix[i, j] = random.Next(-25, 26);
                }
            }

            return matrix;
        }

        public void PrintMatrix(double[,] matrix)
        {
            for (int i = 0; i < matrix.ColCount(); i++)
            {
                for (int j = 0; j < matrix.RowCount(); j++)
                {
                    Console.Write($"{matrix[i,j], 4}");
                }
                Console.WriteLine();
            }
        }

        public bool InputMatrixParams(int col, int row, int userInput)
        {
            if ((col > 0 && row > 0))
            {
                switch (userInput)
                {
                    case 1:
                        return true;

                    case 2:
                        if ((col == row))
                            return true;
                        break;
                }
            }

            return false;
        }

        public void MatrixMultOnNumber(double number, double[,] matrix)
        {
            Console.WriteLine($"{number}A = ");
            for (int i = 0; i < matrix.ColCount(); i++)
            {
                for (int j = 0; j < matrix.RowCount(); j++)
                {
                    matrix[i, j] *= number;
                    Console.Write($" {matrix[i, j],3} ");
                }
                Console.WriteLine();
            }
        }

        public double[,] MatrixSumm(double[,] matrix_1, double[,] matrix_2, Program.Operations op)
        {
            double[,] result = new double[matrix_1.ColCount(), matrix_1.RowCount()];

            for (int i = 0; i < matrix_1.ColCount(); i++)
            {
                for (int j = 0; j < matrix_1.RowCount(); j++)
                {
                    if (op == Program.Operations.Аddition)
                        result[i, j] = matrix_1[i, j] + matrix_2[i, j];
                    if (op == Program.Operations.Subtraction)
                        result[i, j] = matrix_1[i, j] - matrix_2[i, j];
                }
            }

            return result;
        }

        public double[,] MatrixMultiple(double[,] matrix_1, double[,] matrix_2)
        {
            double[,] result = new double[matrix_1.ColCount(), matrix_2.RowCount()];

            for (int i = 0; i < matrix_1.ColCount(); i++)
            {
                for (int j = 0; j < matrix_2.RowCount(); j++)
                {
                    for (var k = 0; k < matrix_2.ColCount(); k++)
                    {
                        result[i, j] += matrix_1[i, k] * matrix_2[k, j];
                    }
                }
            }

            return result;
        }
    }
}
