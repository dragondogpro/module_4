﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_4
{
    static class MatrixExt
    {

        public static int ColCount(this double[,] matrix)
        {
            return matrix.GetUpperBound(0) + 1;
        }

        public static int RowCount(this double[,] matrix)
        {
            return matrix.GetUpperBound(1) + 1;
        }
    }
}
