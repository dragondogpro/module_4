﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module_4
{
    class Note
    {

        public Note(int id, int income, int expenses)
        {
            Id = id;
            Income = income;
            Expenses = expenses;
            Profit = income - expenses;
        }

        public int Id { get; set; }
        public int Income { get; set; }
        public int Expenses { get; set; }
        public int Profit { get; set; }

        public void PrintNote()
        {
            Console.WriteLine($"{Id, 5}{Income, 19}{Expenses, 25}{Profit, 25}");
        }
    }
}
