﻿using Module_4.Libs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Module_4
{
    class Program
    {
        
        const int MONTHS_COUNT = 12;
        const int ELEMENTS_COUNT = 3;

        /// <summary>
        /// Задание 1.
        /// </summary>
        static void Task_1()
        {
            int[] income = Libs.Task_1Lib.GenerateRandomNumbersArray(MONTHS_COUNT);
            int[] expenses = Libs.Task_1Lib.GenerateRandomNumbersArray(MONTHS_COUNT);

            List<Note> noteList = new List<Note>();

            for (int i = 0; i < MONTHS_COUNT; i++)
            {
                noteList.Add(new Note(i + 1,
                                      income[i],
                                      expenses[i]));
            }

            
            // Сортировка листа по полю прибыли
            List<Note> sortedNote = new List<Note>(noteList);
            sortedNote.Sort((emp1, emp2) => emp1.Profit.CompareTo(emp2.Profit));

            List<int> profitsMin = new List<int>
            {
                sortedNote[0].Id
            };

            // Нахождение минимальх элементов с возможными повторениями
            int count = 0;
            for (int i = 1; i < sortedNote.Count(); i++)
            {
                if (sortedNote[i].Profit == sortedNote[i - 1].Profit || count < ELEMENTS_COUNT - 1)
                {
                    profitsMin.Add(sortedNote[i].Id);

                    if (sortedNote[i].Profit != sortedNote[i - 1].Profit)
                        count++;
                }
                else
                    break;
            }

            int positiveMonths = 0;

            // Вывод таблицы в консоль
            Console.WriteLine("Месяц\tДоход, тыс. руб.\tРасход, тыс. руб.\tПрибыль, тыс. руб.");
            foreach (Note note in noteList)
            {
                if (note.Profit > 0)
                    positiveMonths += 1;

                note.PrintNote();
            }

            // Вывод трех месяцев с худшей прибылью
            Console.WriteLine($"\nХудшая прибыль в месяцах: {string.Join(",", profitsMin)}");

            // Вывод количества месяцев с положительной прибылью
            Console.WriteLine($"Месяцев с положительной прибылью: {positiveMonths}");
        }

        const int MAX_COLUMN = 25;

        /// <summary>
        /// Задание 2
        /// </summary>
        static void Task_2()
        {
            Task_2Libs libs = new Task_2Libs();

            int columnCount = 0;
            Console.WriteLine($"Введите необходимое количество строк треугольника Паскаля: (менее {MAX_COLUMN}) ");
            
            // Ввода колличества строк
            while (true)
            {
                columnCount = Convert.ToInt32(Console.ReadLine());

                if (columnCount >= MAX_COLUMN)
                    Console.WriteLine($"Колличество строк должно быть менее {MAX_COLUMN}");
                else
                    break;
            }

            // Печать треугольника в консоль с учетом формулы: k! / (m! * (k - m)!)
            for (int i = 0; i < columnCount; i++)
            {
                for (int k = 0; k <= (columnCount - i); k++)
                {
                        Console.Write("   ");
                }
                for (int k = 0; k <= i; k++)
                {
                    Console.Write($"{libs.Factorial(i) / (libs.Factorial(k) * libs.Factorial(i - k)), 4}   ");
                }
                Console.Write("\n\n");
            }
        }

        public enum Operations
        {
            Аddition = 1,
            Subtraction
        }

        /// <summary>
        /// Задание 3
        /// </summary>
        static void Task_3()
        {
            Task_3Libs libs = new Task_3Libs();
            bool IsAgain = true;

            while (IsAgain)
            {
                Console.WriteLine("Выберете нужный вариант:\n1.Умножение матрицы на число\n2.Сложение/вычитание матриц\n" +
                    "3.Умножение матриц\n4.Выход");

                string userInput = Console.ReadLine();


                int col;
                int row;
                switch (userInput)
                {
                    case "1":
                        Console.WriteLine("Введите колличество строк матрицы");
                        col = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Введите колличество столбцов матрицы");
                        row = Convert.ToInt32(Console.ReadLine());

                        if (libs.InputMatrixParams(col, row, 1))
                        {
                            double[,] matrix = libs.GetMatrixNxN(col, row);
                            libs.PrintMatrix(matrix);

                            Console.WriteLine("Введите число, на которое нужно умножить матрицу: ");
                            double number = Convert.ToDouble(Console.ReadLine());

                            libs.MatrixMultOnNumber(number, matrix);
                        }
                        else
                            Console.WriteLine("Ошибка входных данных!");

                        break;

                    case "2":

                        Operations input;
                        while (true)
                        {
                            Console.WriteLine("1.Сложение\n2.Вычитание");
                            input = (Operations)Convert.ToInt32(Console.ReadLine());

                            if (Enum.IsDefined(typeof(Operations), input))
                                break;
                            else
                                Console.WriteLine("Ошибка входных данных!");
                        }


                        Console.WriteLine("Введите колличество строк матриц");
                        col = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Введите колличество столбцов матриц");
                        row = Convert.ToInt32(Console.ReadLine());

                        if (libs.InputMatrixParams(col, row, 2))
                        {
                            double[,] matrix_1 = libs.GetMatrixNxN(col, row);
                            libs.PrintMatrix(matrix_1);

                            Console.WriteLine();

                            double[,] matrix_2 = libs.GetMatrixNxN(col, row);
                            libs.PrintMatrix(matrix_2);

                            Console.WriteLine();

                            libs.PrintMatrix(libs.MatrixSumm(matrix_1, matrix_2, input));
                        }
                        else
                            Console.WriteLine("Ошибка входных данных!");
                        break;

                    case "3":
                        Console.WriteLine("Введите колличество строк первой матрицы");
                        col = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Введите колличество столбцов первой матрицы");
                        row = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine("Введите колличество строк второй матрицы");
                        int col_2 = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Введите колличество столбцов второй матрицы");
                        int row_2 = Convert.ToInt32(Console.ReadLine());

                        if (libs.InputMatrixParams(row, col_2, 2))
                        {
                            double[,] matrix_1 = libs.GetMatrixNxN(col, row);
                            libs.PrintMatrix(matrix_1);

                            Console.WriteLine();

                            double[,] matrix_2 = libs.GetMatrixNxN(col_2, row_2);
                            libs.PrintMatrix(matrix_2);

                            Console.WriteLine();

                            double[,] result = libs.MatrixMultiple(matrix_1, matrix_2);
                            libs.PrintMatrix(result);
                        }
                        else
                            Console.WriteLine("Ошибка входных данных!");
                        break;

                    case "4":
                        IsAgain = false;
                        break;

                    default:
                        Console.WriteLine("Введено неверное значение");
                        break;
                }
            }            
        }


        static void Main(string[] args)
        {
            bool isAgain = true;

            while (isAgain)
            {
                Console.WriteLine("1.Задание 1\n2.Задание 2\n3.Задание 3\n4.Выход");
                string input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        Task_1();
                        break;

                    case "2":
                        Task_2();
                        break;

                    case "3":
                        Task_3();
                        break;

                    case "4":
                        isAgain = false;
                        break;

                    default:
                        Console.WriteLine("Неверный ввод!");
                        break;
                }

                Console.WriteLine();
            }
        }
    }
}
